import { MigrationInterface, QueryRunner } from "typeorm";
export declare class AddUsernameToUsers1615585463814 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
