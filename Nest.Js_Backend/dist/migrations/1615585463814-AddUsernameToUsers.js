"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddUsernameToUsers1615585463814 = void 0;
class AddUsernameToUsers1615585463814 {
    constructor() {
        this.name = 'AddUsernameToUsers1615585463814';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "users" ADD "username" character varying NOT NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "username"`);
    }
}
exports.AddUsernameToUsers1615585463814 = AddUsernameToUsers1615585463814;
//# sourceMappingURL=1615585463814-AddUsernameToUsers.js.map