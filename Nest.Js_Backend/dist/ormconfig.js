"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_entity_1 = require("./user/user.entity");
const config = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: '123',
    database: 'auth_redux',
    entities: [user_entity_1.UserEntity],
    synchronize: true,
};
exports.default = config;
//# sourceMappingURL=ormconfig.js.map