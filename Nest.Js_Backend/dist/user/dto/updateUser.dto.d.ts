export declare class UpdateUserDto {
    readonly firstname: string;
    readonly lastname: string;
}
