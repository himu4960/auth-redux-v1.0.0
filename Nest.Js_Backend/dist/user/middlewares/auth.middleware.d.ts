import { ExpressRequest } from '@app/types/expressRequest.interface';
import { NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { UserService } from '../user.service';
export declare class AuthMiddleware implements NestMiddleware {
    private readonly userService;
    constructor(userService: UserService);
    use(req: ExpressRequest, _: Response, next: NextFunction): Promise<void>;
}
