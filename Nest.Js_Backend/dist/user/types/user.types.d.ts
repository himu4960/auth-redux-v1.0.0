import { UserEntity } from '../user.entity';
export declare type UserType = Omit<UserEntity, 'hashPassword'>;
