import { UserService } from '@app/user/user.service';
import { CreateUserDto } from './dto/createUser.dto';
import { UserResponseInterface } from './types/userResponse.interface';
import { LoginUserDto } from './dto/loginUser.dto';
import { UserEntity } from './user.entity';
import { UpdateUserDto } from './dto/updateUser.dto';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    createUser(createUserDto: CreateUserDto): Promise<UserResponseInterface>;
    login(loginDto: LoginUserDto): Promise<UserResponseInterface>;
    currentUser(user: UserEntity): Promise<UserResponseInterface>;
    updateCurrentUser(currentUserId: number, updateUserDto: UpdateUserDto): Promise<UserResponseInterface>;
}
