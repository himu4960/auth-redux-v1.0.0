export declare class UserEntity {
    id: number;
    firstname: string;
    lastname: string;
    username: string;
    password: string;
    hashPassword(): Promise<void>;
}
