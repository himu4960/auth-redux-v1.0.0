"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("./user.entity");
const jsonwebtoken_1 = require("jsonwebtoken");
const config_1 = require("../config");
const bcrypt_1 = require("bcrypt");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async createUser(createUserDto) {
        const userByUsername = await this.userRepository.findOne({
            username: createUserDto.username,
        });
        if (userByUsername) {
            throw new common_1.HttpException('username are taken', common_1.HttpStatus.UNPROCESSABLE_ENTITY);
        }
        const newUser = new user_entity_1.UserEntity();
        Object.assign(newUser, createUserDto);
        console.log('newUser', newUser);
        return await this.userRepository.save(newUser);
    }
    async findById(id) {
        return this.userRepository.findOne(id);
    }
    async login(loginUserDto) {
        const user = await this.userRepository.findOne({
            username: loginUserDto.username,
        }, { select: ['id', 'firstname', 'lastname', 'username', 'password'] });
        if (!user) {
            throw new common_1.HttpException('Credentials are not valid', common_1.HttpStatus.UNPROCESSABLE_ENTITY);
        }
        const isPasswordCorrect = await (0, bcrypt_1.compare)(loginUserDto.password, user.password);
        if (!isPasswordCorrect) {
            throw new common_1.HttpException('Credentials are not valid', common_1.HttpStatus.UNPROCESSABLE_ENTITY);
        }
        delete user.password;
        return user;
    }
    async updateUser(userId, updateUserDto) {
        const user = await this.findById(userId);
        Object.assign(user, updateUserDto);
        return await this.userRepository.save(user);
    }
    generateJwt(user) {
        return (0, jsonwebtoken_1.sign)({
            id: user.id,
            firstname: user.firstname,
            lastname: user.lastname,
            username: user.username,
        }, config_1.JWT_SECRET);
    }
    buildUserResponse(user) {
        return {
            user: Object.assign(Object.assign({}, user), { token: this.generateJwt(user) }),
        };
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map