import { ConnectionOptions } from 'typeorm';
import { UserEntity } from './user/user.entity';

const config: ConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: '123',
  database: 'auth_redux',
  entities: [UserEntity],
  synchronize: true,

};

export default config;
