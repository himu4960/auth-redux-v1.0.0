import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ACTION_LOGIN } from '../store/appAction';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  editForm: FormGroup;
  successMessage: String = '';
  firstname='';
  lastname='';
  token :any =localStorage.getItem('token');
  constructor(private _myService:MyserviceService,
    private _router: Router,private _activatedRoute:ActivatedRoute) { 

      this.editForm = new FormGroup({
        firstname: new FormControl('', Validators.required),
        lastname: new FormControl('', Validators.required)
      })


      this._myService.getLoggedInUser(this.token)
      .subscribe(
        data => { 

          this.firstname = this._myService.get(data,'user.firstname'),
          this.lastname = this._myService.get(data,'user.lastname'),
          this.editForm.patchValue({
            firstname:this.firstname,
            lastname:this.lastname
          })

          this._myService.updateState({
            action:ACTION_LOGIN,
            data:data
          })
  
      },
  
        error => this._router.navigate(['/main/login'])
      )


      
    }

  ngOnInit() {

    
  }

  isValid(controlName:any) {
    
    return this.editForm.get(controlName)?.invalid && this.editForm.get(controlName)?.touched
   
  }

  edit() {
    console.log(this.editForm.value);

    var data = {

      "user":{
        "firstname": this.editForm.value.firstname,
        "lastname": this.editForm.value.lastname,
      }
}

    if (this.editForm.valid) {
      this._myService.edit(data)
        .subscribe(
          data => {
           
          
            this.successMessage = "Update Success"
 
            this._router.navigate(['/dash']);
          },
          error => { 
            var error = this. _myService.get(error,'error.message')
            this.successMessage = error;
            this._router.navigate(['/main/login']);
          }
        );
    }
  }
  
  movetodash() {
    this._router.navigate(['../../dash'], { relativeTo: this._activatedRoute });
  }
}
