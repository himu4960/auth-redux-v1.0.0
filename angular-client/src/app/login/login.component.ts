import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ACTION_LOGIN } from '../store/appAction';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  successMessage: String = '';
  constructor(private _myService: MyserviceService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) {
    this.loginForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });

  }

  ngOnInit() {
    this._myService.getAllState().subscribe(
      state=> {
        console.log(state);
      }
    )
  }

  isValid(controlName:any) {
    return this.loginForm.get(controlName)?.invalid && this.loginForm.get(controlName)?.touched
    
  }

  login() {
    var data = {

      "user":{
        "username": this.loginForm.value.username,
        "password": this.loginForm.value.password,
      }
}

    if (this.loginForm.valid) {
      this._myService.login(data)
        .subscribe(
          data => {
           
            var token = this._myService.get(data,"user.token");
           console.log(token);
            localStorage.setItem('token', token.toString()), 
            this.successMessage = "login Success"
            this._router.navigate(['/dash']);
          },
          error => { 
            var error = this. _myService.get(error,'error.message')
            this.successMessage = error;
          }
        );
    }
  }
  
  movetoregister() {
    this._router.navigate(['../register'], { relativeTo: this._activatedRoute });
  }
}
