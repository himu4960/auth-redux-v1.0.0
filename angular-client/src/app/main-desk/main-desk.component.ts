import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-main-desk',
  templateUrl: './main-desk.component.html',
  styleUrls: ['./main-desk.component.css']
})
export class MainDeskComponent implements OnInit {

  constructor(private readonly _myService:MyserviceService) { }

  ngOnInit() {
    this._myService.getAllState().subscribe(
      state=> {
        console.log(state);
      }
    )
  }

}
