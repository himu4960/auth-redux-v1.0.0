import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Store } from '@ngrx/store';

@Injectable()
export class MyserviceService {

  constructor(private _http: HttpClient,private store:Store<any>) { }

  submitRegister(body:any){
    return this._http.post('http://localhost:3000/users/register', body,{
      observe:'body'
    });
  }

  login(body:any){
    return this._http.post('http://localhost:3000/users/login', body,{
      observe:'body'
    });
  }
  edit(body:any){

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    })

    return this._http.put('http://localhost:3000/user', body,{
      observe:'body',
      headers: headers 
    });
  }

  getLoggedInUser(auth_token:string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${auth_token}`
    })
    return this._http.get('http://localhost:3000/user', { headers: headers })
  }
  get<ObjectType>(object: ObjectType, path: string){
    const keys = path.split('.');
    let result:any = object;
    for (const key of keys) {
      result  = result[key];
    }
    return result;
  }

  getAllState(){
    return this.store.select('appReducer');
  }
  

  updateState(obj:any){
   return this.store.dispatch({
     type:obj.action,
     data:obj.data
    })
    
  }

}
