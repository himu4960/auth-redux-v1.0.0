import { ACTION_LOGIN, ACTION_LOGOUT } from "./appAction"

export interface IAppSate{
    id:number,
    firstname:string,
    lastname:string,
    username:string,
    token:string,
    login:boolean

}

export const initialState:IAppSate ={
    id:0,
    firstname:'',
    lastname:'',
    username:'',
    token:'',
    login:false
}

export function rootReducer(state=initialState,action:any):IAppSate{

    switch(action.type){
        case ACTION_LOGIN:
            return {
                ...state,
               login:true,
               id:action.data.user.id,
               firstname:action.data.user.firstname,
               lastname:action.data.user.lastname,
               username:action.data.user.username,
               token:action.data.user.token
    
            }

            case ACTION_LOGOUT:
            return {
                ...state,
                id:0,
                login:false,
                firstname:'',
                lastname:'',
                username:'',
                token:''
            }        
    }

    return state;

}