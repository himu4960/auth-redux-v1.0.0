import { IAppSate, rootReducer } from "./appReducer";

import { ActionReducerMap } from "@ngrx/store";

interface AppState{
    appReducer:IAppSate
}

export const reducers:ActionReducerMap<AppState> = {

    appReducer: rootReducer
}