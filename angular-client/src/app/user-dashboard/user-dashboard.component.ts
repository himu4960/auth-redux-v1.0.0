import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ACTION_LOGIN, ACTION_LOGOUT } from '../store/appAction';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  firstname = '';
  lastname ='';
  token :any =localStorage.getItem('token');
  successMessage: any;
  constructor(private _myService:MyserviceService,
  private _router: Router,private _activatedRoute:ActivatedRoute) { 
    this._myService.getLoggedInUser(this.token)
    .subscribe(
      data => { 
        this._myService.updateState({
          action:ACTION_LOGIN,
          data:data
        })

    },

      error => this._router.navigate(['/main/login'])
    )
  }

  ngOnInit() { 
    this._myService.getAllState().subscribe(
      state=> {
        this.firstname=state.firstname
        this.lastname =state.lastname
      }
    )
  }

  logout(){
    localStorage.removeItem('token');
    this._myService.updateState({
      action:ACTION_LOGOUT,
  
    })

    this._router.navigate(['/main/login']);
  }




  movetoedit() {
      this._myService.getLoggedInUser(this.token)
        .subscribe(
          data => {
            this._router.navigate(['../main/edit'], { relativeTo: this._activatedRoute });
          },
          error => { 
            var error = this. _myService.get(error,'error.message')
            this.successMessage = error;
          }
        );
    
  }
}
